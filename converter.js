//for Arithmetic operation
var newVal;
var plus = document.getElementById("add");
var minus = document.getElementById("sub");
var times = document.getElementById("mul");
var divide = document.getElementById("div");
var demoP=document.getElementById("demo");

function add() {
    var n1 = parseFloat(document.getElementById('num1').value);
    var n2 = parseFloat(document.getElementById('num2').value);
    newVal = n1+n2;
    document.getElementById("demo").innerHTML = "Result =" + newVal ;
}
function sub() {
    var n1 = parseFloat(document.getElementById('num1').value);
    var n2 = parseFloat(document.getElementById('num2').value);
    newVal = n1-n2;
    document.getElementById("demo").innerHTML = "Result =" + newVal ;
}
function mul() {
    var n1 = parseFloat(document.getElementById('num1').value);
    var n2 = parseFloat(document.getElementById('num2').value);
    newVal = n1*n2;
    document.getElementById("demo").innerHTML = "Result =" + newVal ;
}
function div() {
    var n1 = parseFloat(document.getElementById('num1').value);
    var n2 = parseFloat(document.getElementById('num2').value);
    newVal = n1/n2;
    document.getElementById("demo").innerHTML = "Result =" + newVal ;
}
//for currency convertor
function rup(x) {
    x = document.getElementById("rupees").value;
    document.getElementById("dollar").value = x * 0.014;
    document.getElementById("euro").value = x * 0.011;
    document.getElementById("yen").value = x * 1.40;
}
function dol(y) {
    y = document.getElementById("dollar").value;
    document.getElementById("rupees").value = y * 73.97;
    document.getElementById("euro").value = y * 0.85;
    document.getElementById("yen").value = y * 103.44;
}
function eur(z) {
    z = document.getElementById("euro").value;
    document.getElementById("rupees").value = z * 87.47;
    document.getElementById("dollar").value = z * 1.18;
    document.getElementById("yen").value = z * 122.31;
}
function yen(w) {
    w = document.getElementById("yen").value;
    document.getElementById("rupees").value = w * 0.72;
    document.getElementById("dollar").value = w * 0.0097;
    document.getElementById("euro").value = w * 0.0082;
}
//for temparature conversions
function cel(a) {
    a = document.getElementById("celcius").value;
    document.getElementById("fahrenheit").value = (a * 9/5) + 32;
    document.getElementById("kelvin").value = a + 273.15;
}
function fah(b) {
    b = document.getElementById("fahrenheit").value;
    document.getElementById("celcius").value = (b - 32) * 5/9;
    document.getElementById("kelvin").value = ((b - 32) * 5/9) + 273.15;
}
function kel(c) {
    c = document.getElementById("kelvin").value;
    document.getElementById("celcius").value = c - 273.15;
    document.getElementById("fahrenheit").value = ((c - 273.15) *1.8) + 32;
}
